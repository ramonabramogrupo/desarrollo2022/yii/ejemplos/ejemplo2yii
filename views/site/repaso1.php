<?php

use yii\helpers\Html;

foreach ($fotos as $foto){
    echo Html::img("@web/imgs/{$foto}", [
        "alt" => "imagen",
        "class" => "imagen"
    ]);
}
