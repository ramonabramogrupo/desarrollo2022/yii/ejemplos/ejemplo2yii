<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Formulario2 */
/* @var $form ActiveForm */
?>
<div class="site-formulario2">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'texto') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Calcular', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario2 -->
