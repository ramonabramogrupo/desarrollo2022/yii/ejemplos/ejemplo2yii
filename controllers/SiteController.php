<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionEjerepaso2() {
        // llamar a una vista que me muestre titulo y contenido
        
        $datos=[
            "titulo" => "Ejemplo de Datos en MVC",
            "contenido" => "Este contenido se lo pasa el controlador a la vista"
        ];
        
        return $this->render("repaso2",[
            "titulo" => $datos["titulo"],
            "contenido" => $datos["contenido"],
        ]);
    }

    public function actionEjerepaso1() {
        // llamar a una vista que me debe mostrar un par de imagenes
        $fotos=[
            'f1.jpg',
            'f2.jpg'
        ];
        
        return $this->render("repaso1",[
            "fotos" => $fotos,
        ]);
        
    }
    
    public function actionEjeform1(){
        
        // crear un objeto con el modelo Formulario1
        $model = new \app\models\Formulario1();
        
        // al if entra si has pulsado enviar
        if ($model->load(Yii::$app->request->post())) { // cargar los datos del formulario en el modelo
         if ($model->validate()) { //comprobar las reglas del modelo
                return $this->render("resultados",[
                    "model" => $model,
                ]);
            }
        }

        // carga el formulario vacio o con errores
        // de validacion
        return $this->render('formulario1', [
            'model' => $model,
        ]);
    }
    
    public function actionEjeform2(){
        // cargar un formulario que me pida un texto
        // cuando escriba el texto y pulse enviar
        // me indica el numero de caracteres que 
        // tiene el texto
        // 1- crear el modelo Formulario2
        // 2- iros a gii y crear el formulario2 vista
        // 3- Copiar en el gii la accion del controlador
        // 4- pegar la accion aqui
        // 5- crear en el modelo un metodo publico que retorne la longitud del texto escrito
        // 6- Añadir en la accion la vista que muestra resultado (resultados2) y pasarle el modelo
        
        $model = new \app\models\Formulario2();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render("resultados2",[
                    "model" => $model,
                ]);
            }
        }

        return $this->render('formulario2', [
            'model' => $model,
        ]);
    }

}
