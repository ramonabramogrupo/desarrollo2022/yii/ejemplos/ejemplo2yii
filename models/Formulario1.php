<?php

namespace app\models;

use yii\base\Model;


class Formulario1 extends Model{
    public $numero1;
    public $numero2;
    
    public function rules(): array {
        return [
            [['numero1','numero2'],'required'],
            [['numero1','numero2'],'integer','max'=>100]
        ];
    }
    
    public function suma(){
        return $this->numero1+$this->numero2;
    }
    
    public function producto(){
        return $this->numero1*$this->numero2;
    }
    
    public function resultados(){
        return [
          "suma" => $this->suma(),
          "producto" => $this->producto()
        ];
    }
    

}
